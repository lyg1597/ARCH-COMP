function [Rout,Rout_tp,res] = reach_standard(obj,options)
% reach - computes the reachable set for linear systems using the standard
%    (non-wrapping-free) reachability algorithm for linear systems
%
% Syntax:  
%    [Rout,Rout_tp,res] = reach_standard(obj,options)
%
% Inputs:
%    obj - continuous system object
%    options - options for the computation of reachable sets
%
% Outputs:
%    Rout - array of time-interval reachable / output sets
%    Rout_tp - array of time-point reachable / output sets
%    res - true/false whether specification satisfied
%
% Example:
%    ---
%
% References:
%    [1] A. Girard, "Reachability of uncertain linear systems using 
%       zonotopes" in Hybrid Systems: Computation and Control, 
%       ser. LNCS 3414. Springer, 2005, pp. 291--305.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff, Mark Wetzlinger
% Written:       26-June-2019 (from @contDynamics > reach.m)
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

%obtain factors for initial state and input solution
for i=1:(options.taylorTerms+1)
    %compute initial state factor
    options.factor(i) = options.timeStep^(i)/factorial(i);    
end

%if a trajectory should be tracked
if isfield(options,'uTransVec')
    options.uTrans = options.uTransVec(:,1);
else
    inputCorr = 0;
end

% log information
verboseLog(1,options.tStart,options);

%initialize reachable set computations
[Rnext, options] = initReach_Euclidean(obj, options.R0, options);
Rtrans  = options.Rtrans;
Rhom    = options.Rhom;
Rhom_tp = options.Rhom_tp;
Rpar    = options.Rpar;
Raux    = options.Raux;
eAt     = obj.taylor.eAt;

% time period and number of steps
tVec = options.tStart:options.timeStep:options.tFinal;
steps = length(tVec) - 1;
options.i = 1;

% initialize output variables for reachable sets and output sets
Rout = cell(steps,1);
Rout_tp = cell(steps+1,1);

% compute output set of first step
[Rout_tp{1},Rout{1}] = outputSet(obj,options,options.R0,Rnext.ti);
Rstart = Rnext.tp;

% safety property check
if isfield(options,'specification')
    if ~check(options.specification,Rout{1},interval(tVec(1),tVec(2)))
        % violation
        Rout = Rout(1);
        Rout_tp = Rout_tp(1);
        res = false;
        return
    end
end


% loop over all reachability steps
for i = 2:steps
    
    options.i = i;
    
    % post: ----------

    % method implemented from Algorithm 1 in [1]
    
    % if a trajectory should be tracked
    if isfield(options,'uTransVec')
        options.uTrans = options.uTransVec(:,i);
        options.Rhom_tp = Rhom_tp;
        [Rhom, Rhom_tp, ~, inputCorr] = inputInducedUpdates(obj,options);
    else
        Rhom = eAt*Rhom + Rtrans;
        Rhom_tp = eAt*Rhom_tp + Rtrans;
    end
    Rhom = reduce(Rhom,options.reductionTechnique,options.zonotopeOrder);
    Rhom_tp = reduce(Rhom_tp,options.reductionTechnique,options.zonotopeOrder);
    Raux = eAt*Raux;
    Rpar = reduce(Rpar + Raux,options.reductionTechnique,options.zonotopeOrder);
    
    %write results to reachable set struct Rnext
    if isa(Rhom,'mptPolytope')
        Rnext.ti = Rhom + mptPolytope(Rpar) + mptPolytope(inputCorr);
        Rnext.tp = Rhom_tp + mptPolytope(Rpar);
    else
        Rnext.ti = Rhom + zonotope(Rpar) + inputCorr;
        Rnext.tp = Rhom_tp + zonotope(Rpar);
    end
    
    % ----------
    
    % compute output set
    [Rout_tp{i},Rout{i}] = outputSet(obj,options,Rstart,Rnext.ti);
    
    % save reachable set
    Rstart = Rnext.tp;
    
    % safety property check
    if isfield(options,'specification')
        if ~check(options.specification,Rout{i},interval(tVec(i),tVec(i+1)))
            % violation
            Rout = Rout(1:i);
            Rout_tp = Rout_tp(1:i);
            res = false;
            return
        end
    end
    
    % log information
    verboseLog(i,tVec(i),options);
    
end

% compute output set of last set
Rout_tp{end} = outputSet(obj,options,Rstart,Rnext.ti);

% specification fulfilled
res = true;

end

%------------- END OF CODE --------------