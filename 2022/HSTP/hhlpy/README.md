To build the docker image, run the following command in this directory:

```
docker build -t hhlpy-arch-2022 .
``` 

Afterwards, run the following command to run the benchmarks:

```
docker run -t -w /root/mars hhlpy-arch-2022 python3.9 arch.py
```