function val = maxDisplayDim()
% maxDisplayDim - macro for maximum dimension of objects that are fully
%    displayed on the command window; the purpose of this function is to
%    standardize all display functions
%
% Syntax:  
%    val = maxDisplayDim
%
% Inputs:
%    ---
%
% Outputs:
%    val - maximum dimension
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      19-June-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

val = 10;

%------------- END OF CODE --------------
