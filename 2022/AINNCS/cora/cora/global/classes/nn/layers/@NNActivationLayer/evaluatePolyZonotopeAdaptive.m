function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
% evaluates the activation layer on a polyZonotope using polynomials of
% adaptive order
%
% Syntax:
%    [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
%
% Inputs:
%    c, G, Grest, expMat, id, id_, ind, ind_ - parameters of polyZonotope
%    evParams - parameter for NN evaluation
%
% Outputs:
%    updated [c, G, Grest, expMat, id, id_, ind, ind_]
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NNLayer
%
% Author:        Tobias Ladner
% Written:       28-March-2022
% Last update:   05-April-2022
%                23-June-2022 (performance optimizations)
% Last revision: ---

%------------- BEGIN CODE --------------

num_neurons = size(G, 1);
% for image inputs the input dimension is very large, so we
% perform an order reduction here to reduce comp. time
nrGen = evParams.num_generators;
if ~isempty(nrGen) && evParams.i == 2 && ...
        size(G, 2) + size(Grest, 2) > nrGen
    [c, G, Grest, expMat, id] = NNHelper.initialOrderReduction(c, ...
        G, Grest, expMat, id, id_, nrGen);
    id_ = max(max(id), id_);

    ind = find(prod(ones(size(expMat))- ...
        mod(expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(expMat, 2), ind);
end

order = max(obj.order);

% pre order reduction
if evParams.do_pre_order_reduction && ~isempty(evParams.max_gens_post)
    h = size(G, 2);
    q = size(Grest, 2);
    num_gens = nthroot(evParams.max_gens_post, order);
    if h + q > num_gens
        % order reduction
        [c, G, Grest, expMat, id] = NNHelper.initialOrderReduction(c, ...
            G, Grest, expMat, id, id_, num_gens);
        id_ = max(max(id), id_);
        ind = find(prod(ones(size(expMat))- ...
            mod(expMat, 2), 1) == 1);
        ind_ = setdiff(1:size(expMat, 2), ind);
    end
end

% restructure pZ s.t. there remain no independent generators
if size(Grest, 2) > 0 && evParams.remove_Grest && ~isempty(evParams.max_gens_post)
    num_gens = nthroot(evParams.max_gens_post, order);
    num_gens = max(num_neurons, num_gens);
    [c, G, Grest, expMat, id] = NNHelper.restructure(c, G, Grest, expMat, id, id_, num_gens);
    id_ = max(max(id), id_);
    ind = find(prod(ones(size(expMat))- ...
        mod(expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(expMat, 2), ind);

    if ~isempty(Grest)
        error("Grest not entirely removed!")
    end
    Grest = zeros(num_neurons, 0);
end

% initialization
[G_start, G_end, ~, ~] = NNHelper.getOrderIndicesG(G, order);
[~, Grest_end, ~, ~] = NNHelper.getOrderIndicesGrest(Grest, G, order);

% calculate exponential matrix
Es = zeros(size(expMat, 1), G_end(end));
Es_ext = cell(1, order);

Es(:, 1:G_end(1)) = expMat;
Es_ext{1} = expMat;

for i = 2:order
    % Note that e.g., G2 * G3 = G5 -> E2 + E3 = E5
    i1 = floor(i/2);
    i2 = ceil(i/2);

    Ei1_ext = Es_ext{i1};
    Ei2_ext = Es_ext{i2};
    Ei = NNHelper.calcSquaredE(Ei1_ext, Ei2_ext, i1 == i2);
    Ei_ext = [Ei1_ext, Ei2_ext, Ei];

    if i1 < i2
        % free memory
        Es_ext{i1} = [];
    end

    Es(:, G_start(i):G_end(i)) = Ei;
    Es_ext{i} = Ei_ext;
end
clear Es_ext

% init
c_ = zeros(num_neurons, 1);
G_ = zeros(num_neurons, G_end(end));
Grest_ = zeros(num_neurons, Grest_end(end));
d = zeros(num_neurons, 1);

if ~all(size(obj.order) == size(c))
    obj.order = ones(size(c)) .* obj.order;
end

obj.do_refinement = ones(size(c));

% loop over all neurons in the current layer
for j = 1:num_neurons
    evParams.j = j;
    order_j = obj.order(j);
    [c_(j), G_j, Grest_j, d(j)] = ...
        obj.evaluatePolyZonotopeAdaptiveNeuron(c(j), G(j, :), Grest(j, :), expMat, Es, order_j, ...
        ind, ind_, evParams);

    G_(j, 1:length(G_j)) = G_j;
    Grest_(j, 1:length(Grest_j)) = Grest_j;
end

% update properties
c = c_;
G = G_;
Grest = Grest_(:, sum(abs(Grest_), 1) > 0);
expMat = Es;

if ~isempty(nrGen) && nrGen < size(G, 2) + size(Grest, 2)
    % order reduction
    [c, G, Grest, expMat, id, d] = NNHelper.reducePolyZono(c, G, Grest, ...
        expMat, id, d, nrGen);
    id_ = max(max(id), id_);
elseif order > 1
    % exponents remain equal with order = 1
    [expMat, G] = removeRedundantExponents(expMat, G);
end

% save error bound for refinement
obj.refine_metric = d .* obj.do_refinement;

temp = diag(d);
temp = temp(:, d > 0);
if evParams.add_approx_error_to_Grest
    Grest = [Grest, temp];
else
    G = [G, temp];
    expMat = blkdiag(expMat, eye(size(temp, 2)));
    id = [id; 1 + (1:size(temp, 2))' * id_];
    id_ = max(id);
end

% update indices of all-even exponents (for zonotope encl.)
ind = find(prod(ones(size(expMat))-mod(expMat, 2), 1) == 1);
ind_ = setdiff(1:size(expMat, 2), ind);

end