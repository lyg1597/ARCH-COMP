function Hf=hessianTensor_biologicalModel(x,u)



 Hf{1} = sparse(8,8);

Hf{1}(6,1) = -1;
Hf{1}(1,6) = -1;


 Hf{2} = sparse(8,8);



 Hf{3} = sparse(8,8);

Hf{3}(4,3) = -5;
Hf{3}(3,4) = -5;


 Hf{4} = sparse(8,8);

Hf{4}(4,3) = -5;
Hf{4}(3,4) = -5;
Hf{4}(6,5) = 5;
Hf{4}(5,6) = 5;


 Hf{5} = sparse(8,8);

Hf{5}(4,3) = 5;
Hf{5}(3,4) = 5;
Hf{5}(6,5) = -5;
Hf{5}(5,6) = -5;


 Hf{6} = sparse(8,8);

Hf{6}(6,5) = -5;
Hf{6}(5,6) = -5;


 Hf{7} = sparse(8,8);

Hf{7}(6,5) = 5;
Hf{7}(5,6) = 5;
