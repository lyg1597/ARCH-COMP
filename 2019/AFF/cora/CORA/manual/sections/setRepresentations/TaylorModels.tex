Taylor models \cite{Berz1998, Makino1996, Makino2003, Makino2009} can be used to obtain rigorous bounds of functions that are often tighter than the ones obtained by interval arithmetic. To define Taylor models, we first introduce an $n$-dimensional interval $[x]:=[\underline{x},\overline{x}]$, $\forall i: \quad \underline{x}_i \leq \overline{x}_i$, $\underline{x},\overline{x} \in\mathbb{R}^n$. Let us next introduce the multi-index set (see Sec.~3 in \cite{Neidinger2004})
 \begin{equation*}
  \mathcal{L}^q = \Big\{(l_1, l_2, \ldots, l_n) \Big| l_i \in \mathbb{N}, \sum_{i=1}^n l_i \leq q \Big\}.
 \end{equation*}
We define $P^q(x - x_0)$ as the $q$-th order Taylor polynomial of $f(x)$ around $x_0$ ($x,x_0\in\mathbb{R}^n$):
\begin{equation}\label{eq:polynomial}
	P^q(x - x_0) = \sum_{l\in{\mathcal{L}^q}} 
	\frac{(x_1-x_{0,1})^{l_1} \ldots (x_n-x_{0,n})^{l_n}}{l_1! \ldots l_n!} 
	\left.\left( \frac{\partial^{l_1+\ldots+l_n} f(x)}{\partial x_1^{l_1} \ldots \partial x_n^{l_n}}\right)\right|_{x=x0}.
\end{equation}
Let $f : \mathbb{R}^n \to \mathbb{R}^m$ be a function that is $(q + 1)$ times continuously differentiable in an open set containing the $n$-dimensional interval $[x]$. Given $P^q(x - x_0)$ as the $q$-th order Taylor polynomial of $f(x)$ around $x_0 \in [x]$, we choose an n-dimensional interval $[I]$ such that
\begin{equation}
	\forall x\in [x]: \quad f(x) \in P^q(x - x_0) + [I].
	\label{Taylor:TM def}
\end{equation}
The pair $T = (P^q(x - x_0), I)$ is called an $q$-th order Taylor model of $f(x)$ around $x_0$ (see Def.~1 in \cite{Makino2003}). From now on we use the shorthand notation $(P, I)$, and we omit $q$, $x$, and $x_0$ when it is self-evident. Further information can be found in \cite[Sec.~2]{Makino1996}. An illustration of a fourth-order Taylor model is shown in Fig.~\ref{fig:taylor} for $r=\cos(x)$ and the range $[x]=[-\pi/3, \pi/2]$.


\begin{figure}[h]	
	\centering
	\psfrag{x}[c][c]{$x$}						
	\psfrag{y}[c][c]{$r$}	
	\psfrag{a}[c][c]{$\cos(x)$}						
	\psfrag{b}[l][c]{$P^q(x - x_0) + \overline{I}$}	
	\psfrag{c}[r][c]{$P^q(x - x_0) + \underline{I}$}	
	\includegraphics[width=0.7\textwidth]{./figures/taylorModel/taylorModel_cos_b.eps}
	\caption{Fourth-order Taylor model for $\cos(x)$ and $[x]=[-\pi/3, \pi/2]$.}
	\label{fig:taylor}
\end{figure}

A detailed description of how Taylor models are treated in CORA can be found in \cite{Althoff2018b}. In contrast to interval arithmetic and affine arithmetic, Taylor models do not directly provide a range of possible values. The bounds of a Taylor model $T$ with a polynomial $P$ and interval $[I]$ can be over-approximated as $$B(T) = B(P) + [I]$$ using 
\begin{equation} \label{eq:boundOperator}
 B(P^q(x - x_0))=[\min_{x \in [x]} P^q(x - x_0), \max_{x \in [x]} P^q(x - x_0)].
\end{equation}
Several approaches to obtain $B(P)$ exist, out of which \textit{interval arithmetic}, \textit{branch and bound}, and the \textit{LDB/QFB algorithm} are currently implemented in CORA. 

\subsubsection{Creating Taylor Models}

Taylor models are implemented in CORA by the class \texttt{taylm}. To make use of cancellation effects, we have to provide names for variables in order to recognize identical variables; this is different from implementations of interval arithmetic, where each variable is treated individually. We have realized three primal ways to generate a matrix containing Taylor models. 

\paragraph{Method 1: Composition from scalar Taylor models.} The first possibility is to generate scalar Taylor models from intervals as shown subsequently.
{\small
\input{./MATLABcode/example_taylm_method1.tex}}
When a scalar Taylor model is generated from a scalar interval, the name of the variable is deduced from the name of the interval. If one wishes to overwrite the name of a variable \texttt{a2} to \texttt{c}, one can use the command \texttt{taylm(a2, 6, }\{\texttt{'c'}\}\texttt{)}. 

\paragraph{Method 2: Converting an interval matrix.} One can also first generate an interval matrix, i.e., a matrix containing intervals, and then convert the interval matrix into a Taylor model. The subsequent example generates the same Taylor model as in the previous example.
{\small
\input{./MATLABcode/example_taylm_method2.tex}}
Note that the cell for naming variables \{\texttt{'a1';'a2'}\} has to have the same dimensions as the interval matrix \texttt{a}. If no names are provided, default names are automatically generated.

\paragraph{Method 3: Symbolic expressions.} We also provide the possibility to create a Taylor model from a symbolic expression. 
{\small
\input{./MATLABcode/example_taylm_method3.tex}}
This method does not require naming variables since variable names are taken from the variable names of the symbolic expression. The interval of possible values has to be specified after the symbolic expression $s$; here: $[[-2,0]\, [-3, 1]]^T$. 

All examples generate a row vector $c$. Since all variables are normalized to the range $[-1,1]$, we obtain
$$
c =
\begin{bmatrix}
	0.5 + 1.5 \cdot \tilde{a}_1 + [0,0] \\
	2.5 + 0.5 \cdot \tilde{a}_2 + [0,0] \\
\end{bmatrix}
\text{~.}
$$

The following workspace output of MATLAB demonstrates how the dependency problem is considered by keeping track of all encountered variables:
\begin{Verbatim}[samepage=true]
>> c(1) + c(1)
ans = 
	1.0 + 3.0*a1 + [0.00000,0.00000]

>> c(1) + c(2)
ans = 
	3.0 + 1.5*a1 + 0.5*a2 + [0.00000,0.00000]
\end{Verbatim}

\subsubsection{List of Functions of the Class \texttt{taylm}}

In this subsection we list the functions realized in CORA. Since CORA is implemented in MATLAB, the function names are chosen such that they overload the built-in MATLAB functions. Since this class has a lot of methods, we separate them into methods that realize mathematical functions and methods that do not realize mathematical functions. 

\paragraph{Methods realizing mathematical functions and operations}
\begin{itemize}
 \item \texttt{acos} -- $\arccos(\cdot)$ function as defined in \cite[Eq.~(31)]{Althoff2018b}.
 \item \texttt{asin} -- $\arcsin(\cdot)$ function as defined in \cite[Eq.~(30)]{Althoff2018b}.
 \item \texttt{atan} -- $\arctan(\cdot)$ function as defined in \cite[Eq.~(32)]{Althoff2018b}.
 \item \texttt{cos} -- $\cos(\cdot)$ function as defined in \cite[Eq.~(25)]{Althoff2018b}.
 \item \texttt{cosh} -- $\cosh(\cdot)$function as defined in \cite[Eq.~(28)]{Althoff2018b}.
 \item \texttt{det} -- determinant of a Taylor model matrix.
 \item \texttt{exp} -- exponential function as defined in \cite[Eq.~(21)]{Althoff2018b}.
 \item \texttt{interval} -- various implementations of the bound operator $B(\cdot)$ as presented in \cite[Sec.~2.3]{Althoff2018b}.
 \item \texttt{log} -- natural logarithm function as defined in \cite[Eq.~(22)]{Althoff2018b}.
 \item \texttt{minus} -- overloaded '-' operator, see \cite[Eq.~(7)]{Althoff2018b}.
 \item \texttt{mpower} -- overloaded '\string^' operator (power).
 \item \texttt{mrdivide} -- overloaded '/' operator (division), see \cite[Eq.~(9)]{Althoff2018b}.
 \item \texttt{mtimes} -- overloaded '*' operator (multiplication), see \cite[Eq.~(8)]{Althoff2018b} for scalars and \cite[Sec.~2.4]{Althoff2018b} for matrices.
 \item \texttt{plus} -- overloaded '+' operator (addition), see \cite[Eq.~(6)]{Althoff2018b} for scalars and \cite[Sec.~2.4]{Althoff2018b} for matrices.
 \item \texttt{power} -- overloaded '.\string^' operator (elementwise power).
 \item \texttt{rdivide} -- overloads the './' operator: provides elementwise division of two matrices.
 \item \texttt{reexpand} -- re-expand the Taylor model at a new expansion point.
 \item \texttt{sin} -- $\sin(\cdot)$ function as defined in \cite[Eq.~(24)]{Althoff2018b}.
 \item \texttt{sinh} -- $\sinh(\cdot)$ function as defined in \cite[Eq.~(27)]{Althoff2018b}.
 \item \texttt{sqrt} -- $\sqrt{(\cdot)}$ function as defined in \cite[Eq.~(23)]{Althoff2018b}.
 \item \texttt{tan} -- $\tan(\cdot)$ function as defined in \cite[Eq.~(26)]{Althoff2018b}.
 \item \texttt{tanh} -- $\tanh(\cdot)$ function as defined in \cite[Eq.~(29)]{Althoff2018b}.
 \item \texttt{times} -- overloaded '.*' operator for elementwise multiplication of matrices.
 \item \texttt{trace} -- trace of a Taylor model matrix.
 \item \texttt{uminus} --  overloaded '-' operator for a single operand.
 \item \texttt{uplus} --  overloaded '+' operator for a single operand.
\end{itemize}

\paragraph{Other methods}

\begin{itemize}
 \item \texttt{display} -- displays the values of a \texttt{taylm} object in the MATLAB workspace.
 \item \texttt{getCoef} -- returns the array of polynomial coefficients of a \texttt{taylm} object.
 \item \texttt{getRem} -- returns the interval part of a \texttt{taylm} object.		
 \item \texttt{getSyms} -- returns the polynomial part of a \texttt{taylm} object as a symbolic expression.
 \item \texttt{optBnb} -- implementation of the branch and bound algorithm as presented in \cite[Sec.~2.3.2]{Althoff2018b}.
 \item \texttt{optBnbAdv} -- implementation of the advanced branch and bound algorithm as presented in \cite[Sec.~2.3.2]{Althoff2018b}.
 \item \texttt{optLinQuad} -- implementation of the algorithm based on LDB and QFB as presented in \cite[Sec.~2.3.3]{Althoff2018b}.
 \item \texttt{horzcat} -- overloads the operator for horizontal concatenation, e.g., \texttt{a = [b, c, d]}.
 \item \texttt{set} -- set the additional class parameters (see \cite[Sec.~4.3]{Althoff2018b}).
 \item \texttt{setName} -- set the names of the variables in \texttt{taylm}.
 \item \texttt{subsasgn} -- overloads the operator that assigns elements of a \texttt{taylm} matrix \texttt{I}, e.g., \texttt{I(1,2) = value}, where the element of the first row and second column is set.
 \item \texttt{subsref} -- overloads the operator that selects elements of a \texttt{taylm} matrix \texttt{I}, e.g., \texttt{value = I(1,2)}, where the element of the first row and second column is read.
 \item \texttt{taylm} -- constructor of the \texttt{taylm} class.
 \item \texttt{vertcat} -- overloads the operator for vertical concatenation, e.g., \texttt{a = [b; c; d]}.
\end{itemize}


\subsubsection{Additional Parameters for the Class \texttt{taylm}}
\label{subsec:AdditionalParameters}

CORA's Taylor model implementation contains some additional parameters which can be modified by the user:
\begin{itemize}
	\item \textbf{max\_order:} Maximum polynomial degree of the monomials in the polynomial part of the Taylor model. Monomials with a degree larger than \textbf{max\_order} are bounded with the bounding operator $B(\cdot)$ and added to the interval remainder. Further, $q = $ \textbf{max\_order} is used for the implementation of the formulas listed in \cite[Appendix~A]{Althoff2018b}. 
	\item \textbf{tolerance:} Minimum absolute value of the monomial coefficients in the polynomial part of the Taylor model. Monomials with a coefficient whose absolute value is smaller than \textbf{tolerance} are bounded with the bounding operator $B(\cdot)$ and added to the interval remainder.
	\item \textbf{eps:} Termination tolerance $\epsilon$ for the branch and bound algorithm from \cite[Sec.~2.3.2]{Althoff2018b} and for the algorithm based on the Linear Dominated Bounder and the Quadratic Fast Bounder from \cite[Sec.~2.3.3]{Althoff2018b}.
\end{itemize}
These parameters are stored as properties of the class \texttt{taylm}. In the functions \texttt{plus}, \texttt{minus}, and \texttt{times}, two Taylor models are combined to one resulting Taylor model object using the rules
\begin{equation}
\begin{split}
	& \textrm{max\_order}_{new} = \max(\textrm{max\_order}_1, \textrm{max\_order}_2), \\
	& \textrm{tolerance}_{new} = \min(\textrm{tolerance}_1, \textrm{tolerance}_2), \\
	& \textrm{eps}_{new} = \min(\textrm{eps}_1, \textrm{eps}_2),
\end{split}
\end{equation} 
where the subscript $new$ refers to the resulting object, and $1$ and $2$ to the initial objects.

\subsubsection{Taylor Models for Reachability Analysis}
\label{subsec:TaylorModelReachAnalysis}
During reachabilty analysis for nonlinear systems (see Sec. \ref{sec:nonlinearSystems}), the set of abstraction errors $\mathcal{L}$ is obtained by computing upper and lower bounds for the Lagrange remainder function of the Taylor series. This is a classical range bounding problem, and therefore Taylor models can be applied to solve it. Since Taylor models often lead to tighter bounds compared to interval arithmetic, the expection is that the usage of Taylor models leads to a tighter over-approximation of the set of abstraction errors, which then results in a tighter over-approximation of the reachable set. The following algorithm settings enable the evaluation of the set of abstraction errors with Taylor models:
\begin{itemize}
 \item \texttt{options.lagrangeRem.method}: Method that is used to compute the bounds for the set of abstraction errors. The available methods are 'interval' (interval arithmetic), 'taylorModel' or 'zoo'. The default value is 'interval'.
 \item \texttt{options.lagrangeRem.maxOrder}: Maximum polynomial degree of the monomials in the polynomial part of the Taylor model (see Sec. \ref{subsec:AdditionalParameters}).
 \item \texttt{options.lagrangeRem.optMethod}: Method used to calculate the bounds of the Talyor model objects. The available methods are 'int' (interval arithmetic), 'bnb' (branch and bound algorithm), 'bnbAdv' (branch and bound with Taylor model re-expansion) and 'linQuad' (optimization with Linear Dominated Bounder and Quadratic Fast Bounder)
 \item \texttt{options.lagrangeRem.tolerance}: Minimum absolute value of the monomial coefficients in the polynomial part of the Taylor model (see Sec. \ref{subsec:AdditionalParameters}).
 \item \texttt{options.lagrangeRem.eps}: Termination tolerance $\epsilon$ for the branch and bound algorithm and the algorithm based on the Linear Dominated Bounder and the Quadratic Fast Bounder (see Sec. \ref{subsec:AdditionalParameters}).
\end{itemize}   
A list that summarizes all available algorithm settings is provided later on in Section \ref{sec:options}.

\subsubsection{Taylor Model Example}
\label{ap:examples}
This section presents the results of several examples evaluated in CORA:
{\small
\input{./MATLABcode/example_taylm.tex}}

The resulting workspace output is:
\footnotesize
\begin{Verbatim}
B1 = 
 	 0.5 + 1.5*a1 + [0.00000,0.00000]
 	 2.5 + 0.5*a2 + [0.00000,0.00000]

B2 = 
 	 -5.0 + a3 + [0.00000,0.00000]
 	 5.0 + a4 + [0.00000,0.00000]

B1 + B2 = 
 	 -4.5 + 1.5*a1 + a3 + [0.00000,0.00000]
 	 7.5 + 0.5*a2 + a4 + [0.00000,0.00000]

B1' * B2 = 
 	 10.0 - 7.5*a1 + 2.5*a2 + 0.5*a3 + 2.5*a4 + 1.5*a1*a3 + 0.5*a2*a4 + [0.00000,0.00000]

B1 .* B2 = 
 	 -2.5 - 7.5*a1 + 0.5*a3 + 1.5*a1*a3 + [0.00000,0.00000]
 	 12.5 + 2.5*a2 + 2.5*a4 + 0.5*a2*a4 + [0.00000,0.00000]

B1 / 2 = 
 	 0.25 + 0.75*a1 + [0.00000,0.00000]
 	 1.25 + 0.25*a2 + [0.00000,0.00000]

B1 ./ B2 = 
 	 -0.1 - 0.3*a1 - 0.02*a3 - 0.06*a1*a3 - 0.004*a3^2 - 0.012*a1*a3^2 
	  - 0.0008*a3^3 - 0.0024*a1*a3^3 - 0.00016*a3^4 - 0.00048*a1*a3^4 
	  - 0.000032*a3^5 - 0.000096*a1*a3^5 - 6.4e-6*a3^6 + [-0.00005,0.00005]
	  
 	 0.5 + 0.1*a2 - 0.1*a4 - 0.02*a2*a4 + 0.02*a4^2 + 0.004*a2*a4^2 
	  - 0.004*a4^3 - 0.0008*a2*a4^3 + 0.0008*a4^4 + 0.00016*a2*a4^4 
	  - 0.00016*a4^5 - 0.000032*a2*a4^5 + 0.000032*a4^6 + [-0.00005,0.00005]

B1.^3 = 
 	 0.125 + 1.125*a1 + 3.375*a1^2 + 3.375*a1^3 + [0.00000,0.00000]
 	 15.625 + 9.375*a2 + 1.875*a2^2 + 0.125*a2^3 + [0.00000,0.00000]

sin(B1) = 
 	 0.47943 + 1.3164*a1 - 0.53935*a1^2 - 0.49364*a1^3 + 0.10113*a1^4 
	  + 0.055535*a1^5 - 0.0075847*a1^6 + [-0.00339,0.00339]
	  
 	 0.59847 - 0.40057*a2 - 0.074809*a2^2 + 0.01669*a2^3 + 0.0015585*a2^4 
	  - 0.00020863*a2^5 - 0.000012988*a2^6 + [-0.00000,0.00000]

sin(B1(1,1)) + B1(2,1).^2 - B1' * B2 = 
 	 -3.2706 + 8.8164*a1 - 0.5*a3 - 2.5*a4 - 0.53935*a1^2 + 0.25*a2^2 
	  - 1.5*a1*a3 - 0.5*a2*a4 - 0.49364*a1^3 + 0.10113*a1^4 
	  + 0.055535*a1^5 - 0.0075847*a1^6 + [-0.00339,0.00339]
\end{Verbatim}
\normalsize
