function [tp]=transitionProbability_reach(niP,tranFrac,field)
% Purpose:  Calculate the transition probability from the actual cell to
%           the reachable cells
% Pre:      niP (non intersecting polytopes), field
% Post:     transition vector
% Tested:   15.09.06,MA
% Modified: 09.10.06,MA
%           26.03.08,MA
%           29.09.09,MA
%           31.07.17,MA


%initialize--------------------------------------------------------
% nrOfSegments=get(field,'nrOfSegments'); %<-- AP
nrOfSegments=field.nrOfSegments;
tp(1:(prod(nrOfSegments)+1),1)=0; %tp: transition probability
%------------------------------------------------------------------

%get total volume of niPs (non intersecting polytopes)-------------
[tv,pv]=totalVolume(niP);
%------------------------------------------------------------------

%get cells that might intersect with the reachable set-------------
for k=1:length(niP)
    for i=1:length(niP{k})
        if ~iscell(niP{k}{i})
            %polytope conversion if niP{k}{i} is a zonotope
            if strcmp('zonotope',class(niP{k}{i}))
                niP{k}{i}=polytope(niP{k}{i});
            end
            %intersection
            %[tp_total]=cellIntersection2(field,niP{k}{i});
            [iS, iP] = exactIntersectingSegments(field,niP{k}{i});
            % moving calculation of transition probabilites outside the
            % segment intersection method; the tp_total obtained is the
            % same as in partition_old/cellintersection2.m (outside segment
            % is cell index zero)
            tp_total = zeros(field.totalSegments+1,1);
            tp_total((iS+1),1)= iP;
            tp=tp+tranFrac{k}/length(niP{k})*tp_total;
        else
            for j=1:length(niP{k}{i})
                %polytope conversion if niP{k}{i} is a zonotope
                if strcmp('zonotope',class(niP{k}{i}{j}.set))
                    niP{k}{i}{j}.set=polytope(niP{k}{i}{j}.set);
                end
                %intersection
                [tp_total]=cellIntersection2(field,niP{k}{i}{j}.set);
                tp=tp+tranFrac{k}/length(niP{k})*tp_total;
            end
        end
    end
end