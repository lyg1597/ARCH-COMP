function [res, x] = verify(nn, X0, spec, doPlot)
% verify - automated verifciation for specification on neural networks.
%    Note: If the input set is not an interval, a potentially found
%    falsifying example might not be in the actual input set.
%    However, if the verification is successful, also the actual input set
%    is verified.
%
% Syntax:
%    [res, x] = verify(nn, X0, spec)
%    [res, x] = verify(nn, X0, spec, plot)
%
% Inputs:
%    nn - object of class neuralNetwork
%    X0 - initial set of class interval
%    spec - specifications of class specification
%    doPlot - logical; whether the verifications should be plotted
%               (will increase the verification time)
%
% Outputs:
%    res - result: true if specification is satisfied, false if not
%    x - counterexample in terms of an initial point violating the specs
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Niklas Kochdumper, Tobias Ladner
% Written:      23-November-2021
% Last update:  30-November-2022 (TL: removed neuralNetworkOld, adaptive)
% Last revision:---

%------------- BEGIN CODE --------------

if nargin < 4
    doPlot = false;
end

inputArgsCheck({; ...
    {X0, 'att', {'contSet'}}; ...
    {spec, 'att', {'specification'}}; ...
    {doPlot, 'att', {'logical'}}; ...
    });

inputArgsCheck({; ...
    {X0, 'att', {'contSet'}}; ...
    {spec.type, 'str', {'safeSet', 'unsafeSet'}}; ...
    });

% number of refinement steps before splitting the sets
numRefineSteps = 1;

% plot basic information
dimsIn = [1, 2];
dimsOut = [1, 2];

if doPlot
    figure;
    subplot(1, 2, 1);
    hold on;
    title("Input")
    plot(X0, dimsIn, '--', 'Color', colorblind('b'), 'HandleVisibility', 'off');

    xs = [X0.randPoint(1000), X0.randPoint(100, 'extreme')];
    ys = nn.evaluate(xs);

    plot(xs(dimsIn(1), 1), xs(dimsIn(2), 1), '-', 'Color', colorblind('b'), 'DisplayName', 'Unkown')
    plot(xs(dimsIn(1), 1), xs(dimsIn(2), 1), '-', 'Color', colorblind('y'), 'DisplayName', 'Verified')
    scatter(xs(dimsIn(1), :), xs(dimsIn(2), :), '.k', 'DisplayName', 'Samples');
    legend();

    subplot(1, 2, 2);
    hold on;
    title("Output")
    if strcmp(spec.type, 'unsafeSet')
        plot(spec, dimsOut, 'DisplayName', 'Unsafe Specification');
    else
        plot(spec, dimsOut, 'DisplayName', 'Safe Specification');
    end
    plot(ys(dimsOut(1), 1), ys(dimsOut(2), 1), '-', 'Color', colorblind('y'), 'DisplayName', 'Verified')
    scatter(ys(dimsOut(1), :), ys(dimsOut(2), :), '.k', 'DisplayName', 'Samples')
    legend();

    drawnow
end

% transform input to an interval
if ~isa(X0, 'interval')
    X0 = interval(X0);
    warning("CORA Neural Network Verification: "+ ...
        "Input Set is not an interval. "+ ...
        "Thus, a potentially found falsifying example "+ ...
        "might not be in the actual input set. "+ ...
        "However, if the verification is successful, "+ ...
        "also the actual input set is verified.")
end

if doPlot
    subplot(1, 2, 1);
    plot(X0, dimsIn, 'Color', colorblind('b'), 'HandleVisibility', 'off');
    drawnow
end

unkownSets = {X0};

% main evaluation loop
while ~isempty(unkownSets)

    % iterate over all unkown sets
    newUnkownSets = {};
    for i = 1:length(unkownSets)
        X = unkownSets{i};

        % try to falsify set
        [res, x] = aux_falsify(nn, X, spec);
        if ~res % found falsifying point from input set
            if doPlot
                markerSize = 50;
                subplot(1, 2, 1);
                scatter(x(dimsIn(1)), x(dimsIn(2)), markerSize, 'oc', 'filled', 'DisplayName', 'Counterexample')
                subplot(1, 2, 2);
                y = nn.evaluate(x);
                scatter(y(dimsOut(1)), y(dimsOut(2)), markerSize, 'oc', 'filled', 'DisplayName', 'Counterexample')
                drawnow
            end

            return;
        end

        % try to verify set
        [res, Y] = aux_verify(nn, X, spec, numRefineSteps);
        if doPlot
            if res
                subplot(1, 2, 1);
                plot(X, dimsIn, 'Color', colorblind('y'), 'HandleVisibility', 'off');
                subplot(1, 2, 2);
                plot(Y, dimsOut, 'Color', colorblind('y'), 'HandleVisibility', 'off');
                drawnow
            else
                subplot(1, 2, 2);
                % plot(Y, dimsOut, 'Color', colorblind('b'));
                drawnow
            end
        end


        if res
            % set already verified, no further processing required
            continue;
        end

        % split X to get a tighter over-approximation
        [X1, X2] = aux_split(nn, X, spec, Y);
        newUnkownSets = [newUnkownSets, {X1, X2}];

        if doPlot
            subplot(1, 2, 1);
            plot(X1, dimsIn, 'Color', colorblind('b'), 'HandleVisibility', 'off');
            plot(X2, dimsIn, 'Color', colorblind('b'), 'HandleVisibility', 'off');
            drawnow
        end

    end

    % update unkown sets with unkown splitted sets
    unkownSets = newUnkownSets;
end

res = true;
end


% Auxiliary functions -----------------------------------------------------

function [res, x] = aux_falsify(nn, X, spec)
% try to falsify the specifications

res = true;
x = [];

% sample pounts % TODO critical point
xs = [X.randPoint(1000), X.randPoint(100, 'extreme')];
ys = nn.evaluate(xs);

% test points in output space
idx = contains(spec.set, ys);
if strcmp(spec.type, 'unsafeSet')
    if any(idx)
        xs = xs(:, idx);
        x = xs(:, 1);
        res = false;
    end
elseif strcmp(spec.type, 'safeSet')
    if ~all(idx)
        xs = xs(:, ~idx);
        x = xs(:, 1);
        res = false;
    end
else
    throw(CORAerror('CORA:notSupported', ...
        sprintf("Specification of type '%s' is not supported.", ...
        spec.type)) ...
        );
end
end

function [res, Yout] = aux_verify(nn, X, spec, numRefineSteps)
% try to verify the specifications

% TODO: more advanced methods? Niklas had in neuralNetworkOld a more
% advanced version depending on the specific set representation used
% for X and spec. Might be useful to look into. (see remove commit).

% evaluation parameters
evParams = struct;
evParams.reuse_bounds = true;
evParams.num_generators = 10000;

nn.reset();
% evaluate neural network
for i = 1:numRefineSteps
    X = polyZonotope(X);
    Y = nn.evaluate(X, evParams);

    if i == 1
        % return linearized over-approximation if unable to verify
        Yout = Y;
    else
        % TODO: efficient conversion for arbitrary polyZonotopes
        if isa(Y, 'polyZonotope')
            Y = interval(Y, 'split');
        else
            Y = interval(Y);
        end
    end

    % check specification
    res = check(spec, Y);

    if res % verified successfully
        Yout = Y;
        return
    end

    % verify
    nn.refine(5, 'layer', 'both', X.randPoint(1));
end

% unable to verify
res = false;
end

function [X1, X2] = aux_split(nn, X, spec, Y1)
% splits the given set

% TODO: more advanced methods? Niklas had in neuralNetworkOld a more
% advanced version depending on the specific set representation used
% for X and spec. Might be useful to look into. (see remove commit).

type = 'sensitivity';
if strcmp(type, 'longest')
    % split at longest axis
    [~, n] = max(X.sup-X.inf);

elseif strcmp(type, 'sensitivity')
    % split at axis with largest sensitivity
    x = X.randPoint(1);
    S = nn.calcSensitivity(x);
    S = vecnorm(S, 2, 1)' + eps; % eps to avoid 0 sensitivity
    [~, n] = max(S .* (X.sup - X.inf)); % multiply with axis length
else
    throw(CORAerror('CORA:wrongValue', sprintf("Unkown splitting type: %s.", type)))
end

Xs = split(X, n);
X1 = Xs{1};
X2 = Xs{2};
end

%------------- END OF CODE --------------