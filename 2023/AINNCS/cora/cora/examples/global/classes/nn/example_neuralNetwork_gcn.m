function res = example_neuralNetwork_gcn()
% example_neuralNetwork_gcn - example for the verification of a 
%    graph convolutional neural network using the mnist superpixel 
%    data set [1].
%
% Syntax:
%    res = example_neuralNetwork_gcn()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean
%
% References:
%    [1] Monti, F., et al. (2017). Geometric deep learning on graphs and 
%        manifolds using mixture model cnns. IEEE conference on computer 
%        vision and pattern recognition (pp. 5115-5124).
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      24-February-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% load graph convolutional network
gnnpath = [CORAROOT filesep 'models' filesep 'Cora' filesep 'nn' filesep 'examples'];
nnpath = [gnnpath filesep 'gcn_mnist_B1.json'];
nn = neuralNetwork.readGNNnetwork(nnpath);
display(nn)

% load data set
datapath = [gnnpath filesep 'gcn_mnist_data_B1.json'];
data = aux_readData(datapath);

% choose sample
sample = data{16}; % choose 17 for 'UNKNOWN' result
x = sample{1};
adj_list = sample{2}+1; % json is zero indexed
graph = aux_create_graph(adj_list);
y = sample{3}';
y_label = sample{4};

% plot graph
figure;
sgtitle(sprintf("True Label: %d", y_label))
subplot(1, 2, 1)
p = plot(graph, 'EdgeColor', [1 1 1]*0.3, 'NodeLabel',{});
set(gca, 'color', [0 0 0]);
graph.Nodes.NodeColors = x;
p.NodeCData = graph.Nodes.NodeColors;
colormap([1 0 0] .* (0:100)' / 100)
colorbar('southoutside')
caxis([0 1])
xlabel("Feature Weight")
title("Input")
drawnow

% sanity check
disp("Evaluate input:")
evParams = struct;
evParams.graph = graph;
y_pred = nn.evaluate(x, evParams);
res = compareMatrices(y, y_pred, 1e-5);
[~, y_pred_label] = max(y_pred);
fprintf("True Label: %d. Predicted Label: %d\n", y_label, y_pred_label-1)

% add input perturbations
c = x;
delta = 0.001; % adjust perturbation
graph = eye(length(c)) * delta;
Z = zonotope(c, graph);

% evaluate zonotope
disp("Evaluate with perturbations..")
Z_pred = nn.evaluate(Z, evParams);
N = 500;

% evaluate random samples
S = mean(nn.calcSensitivity(c, evParams), 1);
xs = [
    Z.randPoint(N-2), ...
    c + sign(S') * delta, ...
    c - sign(S') * delta
];
ys = nn.evaluate(xs, evParams);

I_pred = interval(Z_pred);
isVerified = (sum(I_pred.sup < I_pred.inf(y_pred_label)) == 9);
res = res && isVerified;

if isVerified
    disp("Result: VERIFIED")
    safeSpecs = '--g';
else
    disp("Result: UNKOWN")
    safeSpecs = '--r';
end


% plot result
disp("Plotting results..")
subplot(1, 2, 2); hold on;
specs = {};
for i=1:10
    plot([I_pred(i); interval(i-1)], [1, 2], 'DisplayName', 'Overapproximation', specs{:})
    scatter(ys(i, :), ones(1, N)*i-1, '.k', 'DisplayName', 'Samples', specs{:})
    specs = {'HandleVisibility', 'off'};
end
plot(I_pred.inf(y_pred_label) * [1 1], [-1, 10], safeSpecs, 'DisplayName', 'Safety Criterion')
ylim([-1 10])
yticks(0:9)
xlabel("Prediction")
ylabel("Prediction Label")
title("Output")
legend('Location', 'northwest')

end

% Auxiliary functions -----------------------------------------------------

function data = aux_readData(datapath)
    fid = fopen(datapath);
    raw = fread(fid,inf);
    str = char(raw');
    fclose(fid);
    data = jsondecode(str);
end

function G = aux_create_graph(adj_list)
    G = graph(adj_list(1, :), adj_list(2, :),'omitselfloops');
    
    % add self loops
    for i=1:height(G.Nodes)
        G = G.addedge(i, i);
    end
end

%------------- END OF CODE --------------
