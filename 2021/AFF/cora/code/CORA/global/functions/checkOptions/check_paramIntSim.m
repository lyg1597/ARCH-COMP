function check_paramIntSim(options, obj)
% check_paramIntSim - checks if options.paramInt
%  1) takes an allowed value
%
% Syntax:
%    check_paramIntSim(options, obj)
%
% Inputs:
%    options   - options for object
%    obj       - system object
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      03-May-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

option = 'paramInt';

if isa(obj,'nonlinParamSys')
    % paramInt must have same length as obj.nrOfParam
    if ~isfield(options,option)
        error(printOptionMissing(obj,option,'params'));
    elseif length(options.paramInt) ~= obj.nrOfParam
        error(printOptionSpecificError(obj,option,...
             'paramInt and obj.nrOfParam must have the same dimension.'));
    elseif ~isa(options.paramInt,'interval')
       options.paramInt = interval(options.paramInt);
    end
end

end

%------------- END OF CODE --------------