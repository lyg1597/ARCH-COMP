function [R,tcomp] = observe_PRadC_II(obj,options)
% observe_PRadC_II - computes the guaranted state estimation approach
% from [1].
%
%
% Syntax:  
%    [R,Rout] = observe_PRadC_II(obj,options)
%
% Inputs:
%    obj - discrete-time linear system object
%    options - options for the guaranteed state estimation
%
% Outputs:
%    R - observed set of points in time
%    Rout - observed set of measurements of points in time
%
% Reference:
%    [1] Ye Wang, Vicenç Puig, and Gabriela Cembrano. Set-
%        membership approach and kalman observer based on
%        zonotopes for discrete-time descriptor systems. Automatica,
%        93:435-443, 2018.
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       18-Sep-2020
% Last update:   04-Jan-2021
%                25-Feb-2021
% Last revision: ---


%------------- BEGIN CODE --------------

% obtain offline gains
options.PRadC_type = 2;
OGain = observe_gain_PRadC(obj,options);

% set intersection procedure
options.intersectionType = 2;
options.intersectionTechnique = OGain; % gain directly provided

% apply set-membership approach
tic
R = observe_stripBased(obj,options);
tcomp = toc;

%------------- END OF CODE --------------