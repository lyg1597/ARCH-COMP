function [R,tcomp] = extremeobserve(obj,params,options)
% extremeobserve - Same setup than observe method.
% Computes the set of possible states of a set-based observer for
% linear systems. Best measurement from past are taking to improve the method applied. 
% (Only works with zonotopes and ellipsoids)
% Syntax:  
%    [R,tcomp] = extremeobserve(obj,options)
%
% Inputs:
%    obj - continuous system object
%    params - model parameters
%    options - options for bounding the set of states
%
% Outputs:
%    R - reachable set of time intervals for the discrete dynamics
%    tcomp - computation time 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Carlos Valero
% Written:       22-Mar-2021
% Last update:   ---
% Last revision: 22-Mar-2021


%------------- BEGIN CODE --------------
tic

aux=params.tStart;
Ts=options.timeStep;
Tf=params.tFinal;
y=params.yVec;
u=params.uTransVec;
t=params.tStart:options.timeStep:params.tFinal;
V=params.V.generators;
v=params.V.center;
no=size(obj.C,1);
IA=inv(obj.A);

R=[];
S=[];
params.yVec=y(:,1:3);
for k = 1:length(t)-2
    params.tStart=aux;
    params.tFinal=aux+2*Ts;
    params.uTransVec=u(:,k:k+2);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [Rf,~] = observe(obj,params,options);
    R = extendReachSetObject(R,Rf);
    aux=aux+Ts;
    params.R0=Rf.timePoint.set{end};
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Past measurement selection
    %
    %center of the set
    T=Rf.timePoint.set{2}.generators;
    thetac=Rf.timePoint.set{2}.center;
    options.sigma = supremum(abs(interval(params.V)));
    no1=size(obj.C,1);
    e=zeros(no1,1);
    % Building strips
    S.P = (obj.C./options.sigma)';
    S.C = params.yVec(:,2)./options.sigma;
    C=obj.C(1:no,:);
    for i=1:no1
        e(i)=sum(-C*(S.P(:,i)'*thetac-S.C(i))/(norm(S.P(:,i)))*S.P(:,i));
    end
    [~,index]=max(e);       %Establish as PM and cM the strip with the major distance
    So.P(:,1)=S.P(:,index);
    So.C(1,1)=S.C(index);
    [~,index]=min(e);       %Establish as PM and cM the strip with the minor distance
    So.P(:,2)=S.P(:,index);
    So.C(2,1)=S.C(index);
    S.P=So.P;
    S.C=So.C;
    %% Strip Propagation
    Tw=params.W.generators;
    [~,m]=size(Tw); %number of generators
    thetaw=params.W.center;
    for i=1:2
        p=S.P(:,i);
        c=S.C(i);
        %Forcing maximum
        for j=1:m
            if p'*Tw(:,j)<0
                Tw(:,j)=-Tw(:,j);
            end
        end
        %Affine transformation
        c=c+p'*IA*obj.B*params.uTransVec(:,2);
        p=(p'*IA)';
        %Minkowski sum between a strip and a Zonotope or Ellipsoid
        aux2=p'*(Tw)*ones(m,1)+1;
        So.C(i,1)=(c+p'*thetaw)/aux2;
        So.P(:,i)=p/aux2;
    end
    obj.C=[C ;So.P'];
    if k<=length(t)-3
        params.yVec=[y(:,k+1:k+3);So.C So.C So.C];
        Tv=[V zeros(2);zeros(2) eye(2)];
        cv=[v;zeros(2,1)];
        params.V=zonotope([cv Tv]);
    end
end
R.timePoint.time=num2cell(t(1:end-1));
R.timeInterval.time=num2cell(t(1:end-1));
tcomp=toc;
end

%------------- END OF CODE --------------
% Auxiliary Functions -----------------------------------------------------

function R = extendReachSetObject(Ru,Rf)
% create and object of class reachSet that stores the reachable set
    if isempty(Ru)
        R=Rf;
    else
        Ru.timePoint.set(end+1)=Rf.timePoint.set(end);
        Ru.timeInterval.set(end+1)=Rf.timeInterval.set(end);  
        R=Ru;
    end
%     timePoint.time = num2cell(tVec);
end


