function f = lotkaVolteraOutside(x,u)

    f(1,1) = 3*x(1) - 3*x(1)*x(2);
    f(2,1) = x(1)*x(2) - x(2);
    f(3,1) = 0;
    f(4,1) = 1;
end